# Generated by Django 3.1.4 on 2021-01-06 18:42

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('courses', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='course',
            name='visible',
            field=models.BooleanField(default=False),
        ),
    ]
