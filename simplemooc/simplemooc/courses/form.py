from django import forms
from simplemooc.core.email import send_email_template
from django.conf import settings

class ContactCourse(forms.Form):
  name = forms.CharField(label='Nome', max_length=100)
  email = forms.EmailField(label='Email')
  message = forms.CharField(label='Mensagem/dúvida',widget=forms.Textarea)

  def send_mail(self, course):
    data = self.cleaned_data
    subject = f'Contato {course}'
    data.update( {
      'course':course,
    })
    send_email_template(subject,"email_duvida.html",data,[settings.CONTACT_EMAIL])
