from simplemooc.core.views import home
from simplemooc.core.views import contact
from django.urls import path


app_name = "core"
urlpatterns = [
    path('', home, name="index"),
    path('contact/', contact, name="contact"),
]