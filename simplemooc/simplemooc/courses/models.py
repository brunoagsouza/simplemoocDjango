from django.db import models
from django.urls import reverse

# Create your models here.
class Course(models.Model):

  name = models.CharField("Nome do curso:", max_length=100)

  slug = models.SlugField("atalho")

  description = models.TextField("Descrição", blank=True)

  about = models.TextField("Sobre o curso", blank=True)

  start_date = models.DateField("data de inicio", auto_now=False, auto_now_add=False, null=True, blank=True)

  image = models.ImageField("imagem do curso:", upload_to='courses/images', null = True, blank = True)

  created_at = models.DateField('Criado em:', auto_now_add=True)

  updated_at = models.DateField("Atualizado em:", auto_now=True)

  visible = models.BooleanField(default=False)

  def __str__(self):
    return self.name

  def get_absolute_url(self):
      return reverse('details', args=[str(self.slug)])
  

  class Meta:

    verbose_name = "Curso"
    verbose_name_plural = "Cursos"
    ordering = ['name']

