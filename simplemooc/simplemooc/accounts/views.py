from django.shortcuts import render,redirect
from simplemooc.accounts.forms import RegisterForm
from django.contrib.auth import authenticate, login
from django.conf import settings
# Create your views here.
def register(request):
  if request.method =='POST':

    form = RegisterForm(request.POST)
    if form.is_valid():
      user = form.save()
      user = authenticate(username=user.username,password=form.cleaned_data['password1'])
      return redirect(settings.LOGIN_REDIRECT_URL)

  else:
    form = RegisterForm()

  context = {
    'form': form
  }


  return render(request,'register.html', context)
