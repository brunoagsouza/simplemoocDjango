from django.contrib.auth.views import LoginView, LogoutView
from simplemooc.accounts.views import register
from django.urls import path

urlpatterns = [
    path('entrar', LoginView.as_view(template_name='login.html')),
    path('cadastro', register, name='register'),
    path('sair',LogoutView.as_view(next_page='core:index'))
]