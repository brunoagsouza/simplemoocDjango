from django.shortcuts import render, get_object_or_404
from .models import Course
from .form import ContactCourse
# Create your views here.
def courses(request):

  courses = Course.objects.filter(visible=True)

  context = {
    'courses':courses
  }

  return render(request,'courses.html',context)

def details(request, slug):
  course = get_object_or_404(Course,slug=slug)
  context = {
    'course':course,
    'form': ContactCourse()
  }

  if request.method =='POST' :

    form = ContactCourse(request.POST)
    if form.is_valid():
      context.update({'is_valid':True})
      form.send_mail(course.name)
      context['form'] = ContactCourse()
      print(form.cleaned_data)

  return render(request,'detail.html', context)