from simplemooc.courses.views import courses
from simplemooc.courses.views import details
from django.urls import path

urlpatterns = [
    path('', courses),
    path('<slug:slug>/',details, name="details")
]