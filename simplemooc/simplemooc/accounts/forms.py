from django import forms
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User

class RegisterForm(UserCreationForm):

  email = forms.EmailField(label="E-mail")

  def save(self):
    user = super(RegisterForm,self).save(commit=False)
    user.email = self.cleaned_data['email']
    user.save()
    return user
  def clean_email(self):
    email = self.cleaned_data['email']
    user = User.objects.filter(email=email).exists()
    if  user:
      raise forms.ValidationError("email já existe")      
    else:
      return email